<?php


namespace App\Enumeration;


class HTTPMethod
{
    public const GET = "GET";
    public const POST = "POST";
    public const PUT = "PUT";
    public const PATCH = "PATCH";
    public const DELETE = "DELETE";
}