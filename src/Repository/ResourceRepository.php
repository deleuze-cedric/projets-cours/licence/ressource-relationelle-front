<?php


namespace App\Repository;


use App\Entity\Ressource;
use App\Enumeration\HTTPMethod;
use App\Service\Curl;

class ResourceRepository
{
    const URL_RESOURCE = '/ressources';

    /**
     * @param String $slug
     * @return Ressource
     * @throws \Exception
     */
    public function getResource(string $slug): ?Ressource
    {
        $curl = new Curl($_SERVER['API_URL'] . self::URL_RESOURCE . '/' . $slug, HTTPMethod::GET);
        $resource = $curl->execute();
        return new Ressource($resource);
    }

}