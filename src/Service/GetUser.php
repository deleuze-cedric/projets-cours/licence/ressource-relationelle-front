<?php


namespace App\Service;


use App\Entity\User;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;

class GetUser
{

    private RequestStack $requestStack;

    /**
     * GetUser constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function checkUserExist()
    {
        $userJSON = $this->requestStack->getCurrentRequest()->cookies->get($_SERVER['COOKIE_VAR_USER']);
        if (!empty($userJSON)) {
            try {
                $user = json_decode($userJSON);
                if (!empty($user->uuid)) {
                    return true;
                }
            } catch (Exception $e) {

            }
        }
        return false;
    }

    /**
     * @return User
     */
    public function getUserFromCookie()
    {
        $userJSON = $this->requestStack->getCurrentRequest()->cookies->get($_SERVER['COOKIE_VAR_USER']);
        $user = null;

        try {
            $user = new User();
            if (!empty($userJSON)) {
                $user->createFromJson($userJSON);
            }
        } catch (Exception $e) {
        }

        return $user;

    }
}