<?php


namespace App\Service;


use App\Enumeration\HTTPContentType;
use App\Enumeration\HTTPMethod;
use Exception;

class Curl
{
    /**
     * @var String
     */
    protected String $url;

    /**
     * @var string
     */
    protected string $method;

    /**
     * @var array|null
     */
    protected $customHeader;

    /**
     * @var bool
     */
    protected bool $followLocation;

    /**
     * @var bool
     */
    protected bool $returnTransfer;

    /**
     * @var string
     */
    protected string $contentType;

    /**
     * Curl constructor.
     * @param String $url
     * @param HTTPMethod|string $method
     * @param string $contentType
     * @param bool $followLocation
     * @param bool $returnTransfer
     * @param array|null $customHeader
     */
    public function __construct(string $url, string $method, string $contentType = HTTPContentType::JSON_LD, bool $followLocation = true, bool $returnTransfer = true, array $customHeader = null)
    {
        $this->url = $url;
        $this->method = $method;
        $this->customHeader = $customHeader;
        $this->followLocation = $followLocation;
        $this->returnTransfer = $returnTransfer;
        $this->contentType = $contentType;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function execute()
    {

        // Initialisation de la requête
        $ch = curl_init($this->url);

        // Soit il y un header soit on le créé
        $header = $this->customHeader ?? [
                'Content-type: ' . $this->contentType
            ];

        $options = [
            CURLOPT_FOLLOWLOCATION => $this->followLocation,
            CURLOPT_RETURNTRANSFER => $this->returnTransfer,
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_HTTPHEADER => $header
        ];

        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if(!in_array($info['http_code'], [200, 201, 202, 203])){
            throw new Exception(json_encode($info));
        }

        return $res;
    }

}