<?php

namespace App\Security;

use App\Entity\User;
use App\Service\GetUser;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{

    private GetUser $getUser;

    /**
     * UserProvider constructor.
     * @param GetUser $getUser
     */
    public function __construct(GetUser $getUser)
    {
        $this->getUser = $getUser;
    }

    public function loadUserByUsername($username)
    {
        return $this->getUser->getUserFromCookie();
    }

    public function refreshUser(UserInterface $user)
    {
        return $this->getUser->getUserFromCookie();
    }

    public function supportsClass($class)
    {
        return User::class == $class;
    }
}