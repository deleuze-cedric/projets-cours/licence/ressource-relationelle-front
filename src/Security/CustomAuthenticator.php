<?php

namespace App\Security;

use App\Service\GetUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class CustomAuthenticator extends AbstractGuardAuthenticator
{
    private GetUser $getUser;
    private UrlGeneratorInterface $urlGenerator;
    private SessionInterface $session;

    /**
     * UserProvider constructor.
     * @param GetUser $getUser
     * @param UrlGeneratorInterface $urlGenerator
     * @param SessionInterface $session
     */
    public function __construct(GetUser $getUser, UrlGeneratorInterface $urlGenerator, SessionInterface $session)
    {
        $this->urlGenerator = $urlGenerator;
        $this->getUser = $getUser;
        $this->session = $session;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     * @param Request $request
     * @return bool|null
     */
    public function supports(Request $request): ?bool
    {
        return !empty($request->cookies->get($_SERVER['COOKIE_VAR_USER']));
    }

    public function authenticate(Request $request): PassportInterface
    {
        $user = $this->getUser->getUserFromCookie();

        if (!empty($user)) {
            return new SelfValidatingPassport($user);
        } else {
            throw new CustomUserMessageAuthenticationException('Erreur lors de l\'authentifaction');
        }
    }

    public function getCredentials(Request $request)
    {
        return [];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$this->getUser->checkUserExist()) {
            return null;
        }
        return $this->getUser->getUserFromCookie();
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return null;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $routeName = $request->getRequestUri();
        return new RedirectResponse($this->urlGenerator->generate('user_login') . "?redirect=" . $routeName);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}