<?php

namespace App\Entity;

use DateTime;

class Commentaire
{

    protected String $contenu;

    /**
     * @var Ressource|String
     */
    protected $ecritPour;

    /**
     * @var User|String
     */
    protected $ecritPar;

    /**
     * @var CommentaireModel|String|null
     */
    protected $commentaireParent;

    protected array $lsCommentairesEnfants = [];

    protected DateTime $createdDate;

    protected DateTime $updatedDate;

    protected DateTime $activatedDate;

    protected DateTime $deletedDate;

    /**
     * @return String
     */
    public function getContenu(): string
    {
        return $this->contenu;
    }

    /**
     * @return Ressource|String
     */
    public function getEcritPour()
    {
        return $this->ecritPour;
    }

    /**
     * @return User|String
     */
    public function getEcritPar()
    {
        return $this->ecritPar;
    }

    /**
     * @return CommentaireModel|String|null
     */
    public function getCommentaireParent()
    {
        return $this->commentaireParent;
    }

    /**
     * @return array
     */
    public function getLsCommentairesEnfants(): array
    {
        return $this->lsCommentairesEnfants;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @return DateTime
     */
    public function getActivatedDate(): DateTime
    {
        return $this->activatedDate;
    }

    /**
     * @return DateTime
     */
    public function getDeletedDate(): DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param String $contenu
     */
    public function setContenu(string $contenu): void
    {
        $this->contenu = $contenu;
    }

    /**
     * @param Ressource|String $ecritPour
     */
    public function setEcritPour($ecritPour): void
    {
        $this->ecritPour = $ecritPour;
    }

    /**
     * @param User|String $ecritPar
     */
    public function setEcritPar($ecritPar): void
    {
        $this->ecritPar = $ecritPar;
    }

    /**
     * @param CommentaireModel|String|null $commentaireParent
     */
    public function setCommentaireParent($commentaireParent): void
    {
        $this->commentaireParent = $commentaireParent;
    }


}
