<?php

namespace App\Entity;

use DateTime;

class Partage extends Entity
{
    /**
     * @var User|String
     */
    protected $partageur;

    /**
     * @var TypeRelation|String
     */
    protected $ciblePartage;

    /**
     * @var Ressource|String
     */
    protected $ressourcePartage;

    protected DateTime $createdDate;

    protected DateTime $activatedDate;

    /**
     * @return User|String
     */
    public function getPartageur()
    {
        return $this->partageur;
    }

    /**
     * @return TypeRelation|String
     */
    public function getCiblePartage()
    {
        return $this->ciblePartage;
    }

    /**
     * @return Ressource|String
     */
    public function getRessourcePartage()
    {
        return $this->ressourcePartage;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getActivatedDate(): DateTime
    {
        return $this->activatedDate;
    }

    /**
     * @param User|String $partageur
     */
    public function setPartageur($partageur): void
    {
        $this->partageur = $partageur;
    }

    /**
     * @param TypeRelation|String $ciblePartage
     */
    public function setCiblePartage($ciblePartage): void
    {
        $this->ciblePartage = $ciblePartage;
    }

    /**
     * @param Ressource|String $ressourcePartage
     */
    public function setRessourcePartage($ressourcePartage): void
    {
        $this->ressourcePartage = $ressourcePartage;
    }

}
