<?php

namespace App\Entity;

use DateTime;

class Relation
{
    /**
     * @var TypeRelation|String
     */
    protected $typeRelation;

    /**
     * @var User|String
     */
    protected $demandeur;

    /**
     * @var User|String
     */
    protected $receveur;

    protected DateTime $dateDemande;

    protected DateTime $acceptedDate;

    /**
     * @return TypeRelation|String
     */
    public function getTypeRelation()
    {
        return $this->typeRelation;
    }

    /**
     * @return User|String
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }

    /**
     * @return User|String
     */
    public function getReceveur()
    {
        return $this->receveur;
    }

    /**
     * @return DateTime
     */
    public function getDateDemande(): DateTime
    {
        return $this->dateDemande;
    }

    /**
     * @return DateTime
     */
    public function getAcceptedDate(): DateTime
    {
        return $this->acceptedDate;
    }

    /**
     * @param TypeRelation|String $typeRelation
     */
    public function setTypeRelation($typeRelation): void
    {
        $this->typeRelation = $typeRelation;
    }

    /**
     * @param User|String $demandeur
     */
    public function setDemandeur($demandeur): void
    {
        $this->demandeur = $demandeur;
    }

    /**
     * @param User|String $receveur
     */
    public function setReceveur($receveur): void
    {
        $this->receveur = $receveur;
    }


}
