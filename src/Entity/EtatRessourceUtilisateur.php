<?php

namespace App\Entity;

use DateTime;

class EtatRessourceUtilisateur extends Entity
{
    /**
     * @var Ressource|String
     */
    protected $ressource;

    /**
     * @var User|String
     */
    protected $utilisateur;

    /**
     * @var EtatRessource|String
     */
    protected $etatRessource;

    protected DateTime $createdDate;

    /**
     * @return Ressource|String
     */
    public function getRessource()
    {
        return $this->ressource;
    }

    /**
     * @return User|String
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * @return EtatRessource|String
     */
    public function getEtatRessource()
    {
        return $this->etatRessource;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param Ressource|String $ressource
     */
    public function setRessource($ressource): void
    {
        $this->ressource = $ressource;
    }

    /**
     * @param User|String $utilisateur
     */
    public function setUtilisateur($utilisateur): void
    {
        $this->utilisateur = $utilisateur;
    }

    /**
     * @param EtatRessource|String $etatRessource
     */
    public function setEtatRessource($etatRessource): void
    {
        $this->etatRessource = $etatRessource;
    }

}
