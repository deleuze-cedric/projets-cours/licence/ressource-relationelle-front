<?php

namespace App\Entity;

use DateTime;
use stdClass;

class Ressource extends Entity
{

    protected String $slug;

    protected String $titre;

    protected stdClass $contenu;

    /**
     * @var CategorieRessource|String
     */
    protected $categorieRessource;

    /**
     * @var TypeRessource|String
     */
    protected $typeRessource;

    /**
     * @var User|String
     */
    protected $createur;

    protected array $lsPartages = [];

    protected DateTime $createdDate;

    protected ?DateTime $updatedDate;

    protected ?DateTime $deletedDate;

    /**
     * @var MediaObject|String
     */
    private $image;

    /**
     * @return String
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return String
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return ?stdClass
     */
    public function getContenu(): ?stdClass
    {
        return $this->contenu;
    }

    /**
     * @return CategorieRessource|String
     */
    public function getCategorieRessource()
    {
        return $this->categorieRessource;
    }

    /**
     * @return TypeRessource|String
     */
    public function getTypeRessource()
    {
        return $this->typeRessource;
    }

    /**
     * @return User|String
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * @return array
     */
    public function getLsPartages(): array
    {
        return $this->lsPartages;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @return DateTime
     */
    public function getDeletedDate(): DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @return MediaObject|String
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param String $titre
     */
    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @param String $contenu
     */
    public function setContenu(string $contenu): void
    {
        $this->contenu = $contenu;
    }

    /**
     * @param CategorieRessource|String $categorieRessource
     */
    public function setCategorieRessource($categorieRessource): void
    {
        $this->categorieRessource = $categorieRessource;
    }

    /**
     * @param TypeRessource|String $typeRessource
     */
    public function setTypeRessource($typeRessource): void
    {
        $this->typeRessource = $typeRessource;
    }

    /**
     * @param User|String $createur
     */
    public function setCreateur($createur): void
    {
        $this->createur = $createur;
    }

    /**
     * @param MediaObject|String $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

}
