<?php


namespace App\Entity;


use DateTime;
use Exception;

abstract class Entity
{

    /**
     * Entity constructor.
     * @param ?String $json
     */
    public function __construct(string $json = null)
    {
        if (!empty($json)) {
            $this->createFromJson($json);
        }
    }

    /**
     * @param string $json
     * @return bool
     */
    public function createFromJson(string $json)
    {
        try {

            $obj = json_decode($json);

            foreach ($obj as $property => $value) {
                // Vérification du DateTime
                $date = $this->getDateTime($value);

                if (!empty($value) && $date instanceof DateTime) {
                    $this->{$property} = $date;
                } else {
                    $this->{$property} = $value;
                }
            }

            return true;

        } catch (Exception $e) {

        }

        return false;

    }

    public function getDateTime($date)
    {
        try {

            $format = 'Y-m-d\TH:i:sP';
            $datetime = DateTime::createFromFormat($format, $date);

            if (!$datetime) {
                $format = 'Y-m-d H:i:s';
                $datetime = DateTime::createFromFormat($format, $date);

                if (!$datetime) {
                    $format = 'Y-m-d';
                    $datetime = DateTime::createFromFormat($format, $date);
                }
            }

            return $datetime;

        } catch (Exception $e) {
        }
        return $date;
    }

}