<?php

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\File;

class MediaObject extends Entity
{

    protected String $contentUrl;

    protected  File $file;

    protected String $filePath;

    /**
     * @return String
     */
    public function getContentUrl(): string
    {
        return $this->contentUrl;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return String
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param String $contentUrl
     */
    public function setContentUrl(string $contentUrl): void
    {
        $this->contentUrl = $contentUrl;
    }

    /**
     * @param File $file
     */
    public function setFile(File $file): void
    {
        $this->file = $file;
    }

    /**
     * @param String $filePath
     */
    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }


}