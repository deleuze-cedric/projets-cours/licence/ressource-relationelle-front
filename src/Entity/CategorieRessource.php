<?php

namespace App\Entity;

use DateTime;

class CategorieRessource extends Entity
{

    protected String $slug;

    protected String $libelle;

    protected DateTime $createdDate;

    protected DateTime $updatedDate;

    /**
     * @var MediaObject|String
     */
    protected $logo;

    /**
     * @return String
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @param String $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return MediaObject
     */
    public function getLogo(): MediaObject
    {
        return $this->logo;
    }

    /**
     * @param MediaObject $logo
     */
    public function setLogo(MediaObject $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return String
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

}
