<?php

namespace App\Entity;

use DateTime;

class Message extends Entity
{

    protected String $contenu;

    /**
     * @var User|String
     */
    protected $auteur;

    /**
     * @var Discussion|String
     */
    protected $discussion;

    /**
     * @var Message|String
     */
    protected $messageParent;

    /**
     * @var Message|String
     */
    protected $messageEnfants;

    protected DateTime $createdDate;

    protected DateTime $updatedDate;

    protected DateTime $deletedDate;

    /**
     * @return String
     */
    public function getContenu(): string
    {
        return $this->contenu;
    }

    /**
     * @return User|String
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @return Discussion|String
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @return Message|String
     */
    public function getMessageParent()
    {
        return $this->messageParent;
    }

    /**
     * @return Message|String
     */
    public function getMessageEnfants()
    {
        return $this->messageEnfants;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedDate(): DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @return DateTime
     */
    public function getDeletedDate(): DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param String $contenu
     */
    public function setContenu(string $contenu): void
    {
        $this->contenu = $contenu;
    }

    /**
     * @param User|String $auteur
     */
    public function setAuteur($auteur): void
    {
        $this->auteur = $auteur;
    }

    /**
     * @param Discussion|String $discussion
     */
    public function setDiscussion($discussion): void
    {
        $this->discussion = $discussion;
    }

    /**
     * @param Message|String $messageParent
     */
    public function setMessageParent($messageParent): void
    {
        $this->messageParent = $messageParent;
    }



}
