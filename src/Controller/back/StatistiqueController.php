<?php


namespace App\Controller\back;


use App\Controller\Controller;
use http\Env\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatistiqueController
 * @package App\Controller\admin
 * @Route("/admin/statistiques", name="admin_statistiques_")
 */
class StatistiqueController extends AdminController
{

    /**
     * @Route(name="page")
     */
    public function admin(){

        return $this->render('admin/accueil/index.html.twig');

    }

}