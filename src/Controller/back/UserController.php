<?php


namespace App\Controller\back;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RessourceController
 * @package App\Controller\back
 * @Route("/admin/utilisateurs", name="admin_user_")
 */
class UserController extends AdminController
{
    /**
     * @Route(name="list")
     */
    public function list(): Response
    {
        return $this->render('admin/users/list.html.twig', []);
    }

    /**
     * @Route("/creation", name="create")
     */
    public function create(): Response
    {
        return $this->render('admin/users/form.html.twig', []);
    }

}