<?php


namespace App\Controller\back;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RessourceController
 * @package App\Controller\back
 * @Route("/admin/ressources", name="admin_resource_")
 */
class RessourceController extends AdminController
{
    /**
     * @Route(name="list")
     */
    public function list(): Response
    {
        return $this->render('admin/ressources/list.html.twig', ["multipleSelect" => true]);
    }

    /**
     * @Route("/creation", name="create")
     */
    public function create(): Response
    {
        return $this->render('admin/ressources/form.html.twig', ["multipleSelect" => true]);
    }

    /**
     * @Route("/validation" , name="valide")
     */
    public function valide(): Response
    {
        return $this->render('admin/ressources/valide.html.twig', ["multipleSelect" => true]);
    }

}