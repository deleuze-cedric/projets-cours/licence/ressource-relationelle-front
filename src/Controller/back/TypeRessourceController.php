<?php


namespace App\Controller\back;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RessourceController
 * @package App\Controller\back
 * @Route("/admin/types_ressources", name="admin_type_ressource_")
 */
class TypeRessourceController extends AdminController
{
    /**
     * @Route(name="list")
     */
    public function list(): Response
    {
        return $this->render('admin/typesRessource/list.html.twig', []);
    }

    /**
     * @Route("/creation", name="create")
     */
    public function create(): Response
    {
        return $this->render('admin/typesRessource/form.html.twig', []);
    }

}