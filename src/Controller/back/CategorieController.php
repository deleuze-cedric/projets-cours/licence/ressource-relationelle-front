<?php


namespace App\Controller\back;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RessourceController
 * @package App\Controller\back
 * @Route("/admin/categories", name="admin_category_")
 */
class CategorieController extends AdminController
{
    /**
     * @Route(name="list")
     */
    public function list(): Response
    {
        return $this->render('admin/categories/list.html.twig', []);
    }

    /**
     * @Route("/creation", name="create")
     */
    public function create(): Response
    {
        return $this->render('admin/categories/form.html.twig', []);
    }

}