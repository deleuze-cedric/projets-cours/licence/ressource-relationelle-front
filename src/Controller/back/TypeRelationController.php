<?php


namespace App\Controller\back;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RessourceController
 * @package App\Controller\back
 * @Route("/admin/types_relations", name="admin_types_relations_")
 */
class TypeRelationController extends AdminController
{
    /**
     * @Route(name="list")
     */
    public function list(): Response
    {
        return $this->render('admin/typeRelations/list.html.twig', []);
    }

    /**
     * @Route("/creation", name="create")
     */
    public function create(): Response
    {
        return $this->render('admin/typeRelations/form.html.twig', []);
    }

}