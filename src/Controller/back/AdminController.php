<?php


namespace App\Controller\back;


use App\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\admin
 * @Route("/admin", name="admin_")
 */
class AdminController extends Controller
{

    /**
     * @Route(name="home")
     */
    public function admin(){

        return $this->render('admin/accueil/index.html.twig');

    }

}