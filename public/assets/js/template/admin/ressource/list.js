// JQuery
let $tableResource;
let $selectTypeRessource;
let $selectCategorieRessource;
let $btnSearchResources;
let $searchUser;
let $searchTitle;
let $sortRes;

// JS
let pagination;
let nbItermPerPage = 10;
let multipleSelectCateg;
let multipleSelectType;
let table;

$(document).ready(function () {

    // Initialisation du model
    ressourceModel = new RessourceModel();

    table = new Table('table-admin-resources');

    pagination = new Pagination(nbItermPerPage);
    pagination.addEventListener(Pagination.pageChangeEvent, filterResource);

    // ----------------------- FACETS ------------------------------

    // Récupération du bouton forçant la recherche
    $btnSearchResources = $('#btn-search-resources');
    // Au click sur le bouton on filtre les résultats
    $btnSearchResources.click(function () {
        filterResource(false, null, true);
    });

    // ----------------------- AJAX ------------------------------

    // Initialisation des différents models
    let categorieRessource = new CategorieRessourceModel();
    let typeRessource = new TypeRessource();

    $selectTypeRessource = $('#select-type-ressource');
    $selectCategorieRessource = $('#select-categorie-ressource');
    $searchUser = $('#search-user');
    $searchTitle = $('#search-title');
    $sortRes = $('#sort-res');

    // Paramètres envoyés pour la requête api
    let params = {};
    params[Model.LOADER] = "global-loader";

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les catégories de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        let idSelect = "select-categorie-ressource";
        let select = Model.parseDataToSelect(Model.getListFromJSONLD(data), "slug", "libelle", idSelect, "", "", true);
        $(select).find('option').prop('selected', true);
        $selectCategorieRessource.replaceWith(select);
        // Transformation du select en multiple select
        multipleSelectCateg = new MultipleSelect();
        multipleSelectCateg.id = idSelect;
        multipleSelectCateg.parameter = {'locale': 'fr-FR'}
        multipleSelectCateg.initMultipleSelect();
        multipleSelectCateg.close();
    };
    // Récupération de la liste des catégories ressources
    categorieRessource.callAction(Model.LIST, params);

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les types de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        let idSelect = "select-type-ressource";
        let select = Model.parseDataToSelect(Model.getListFromJSONLD(data), "slug", "libelle", idSelect, "", "", true);
        $(select).find('option').prop('selected', true);
        $selectTypeRessource.replaceWith(select);
        // Transformation du select en multiple select
        multipleSelectType = new MultipleSelect();
        multipleSelectType.id = idSelect;
        multipleSelectType.parameter = {'locale': 'fr-FR'}
        multipleSelectType.initMultipleSelect();
        multipleSelectType.close();
    };
    // Récupération de la liste des types de ressources
    typeRessource.callAction(Model.LIST, params);

    // ----------------------- Ressource ------------------------------

    // Premier filtrage
    filterResource(true, false);
});

/**
 * Permet de filtrer les ressources
 *
 * @param init
 * @param forceSearch
 */
function filterResource(init, forceSearch) {

    // Exécution de la fonction de recherche
    ressourceModel.filterResources(
        // Fonction appellée au succès de la requête
        function (data, status) {
            pagination.calculNbPage(data);
            table.bindTable(data, createRowResource);
        },
        // Page à récupérer
        pagination.currentPage + 1,
        // Nombre d'item par page
        nbItermPerPage,
        // Récupération des types relations sélectionnées
        null,
        // Récupération des types ressource sélectionnées
        getParamUriTypeRessource(),
        // Récupération des catégories ressource sélectionnées
        getParamUriCategorieRessource(),
        // Récupération du filtre par utilisateur
        getParamUriUser(),
        // Récupération du filtre par titre
        getParamUriTitle()
    );
}

/**
 * Récupère les checkbox check pour les categories
 *
 * @returns {string|[]}
 */
function getParamUriCategorieRessource() {
    let categoriesRessourcesSelected = $selectCategorieRessource.val();

    // Si tous inputs sont sélectionnés on retourne rien
    if (!categoriesRessourcesSelected || categoriesRessourcesSelected.length === $selectCategorieRessource.find('option').length) {
        return "";
    }

    return categoriesRessourcesSelected;
}

/**
 * Récupère les checkbox check pour les types
 *
 * @returns {string|[]}
 */
function getParamUriTypeRessource() {
    let typeRessourceSelected = $selectTypeRessource.val();

    // Si tous inputs sont sélectionnés on retourne rien
    if (!typeRessourceSelected || typeRessourceSelected.length === $selectTypeRessource.find('option').length) {
        return "";
    }

    return typeRessourceSelected;
}

/**
 * Récupère la valeur dans le input de choix de l'utilisateur
 *
 * @returns {string|[]}
 */
function getParamUriUser() {
    let val = $searchUser.val();
    if (val && val.trim()) {
        return val;
    }
    return null;
}

/**
 * Récupère la valeur dans le input du titre
 *
 * @returns {string|[]}
 */
function getParamUriTitle() {
    let val = $searchTitle.val();
    if (val && val.trim()) {
        return val;
    }
    return null;
}

function createRowResource(data) {
    let tr = document.createElement('tr');

    let tdTitle = document.createElement('td');
    let titre = data.titre;
    tdTitle.innerText = titre;
    tdTitle.title = titre;
    tr.append(tdTitle);

    let tdCreator = document.createElement('td');
    let creator = data.createur.nom + " " + data.createur.prenom;
    if (data.createur.pseudo) {
        creator += " (" + data.createur.pseudo + ")";
    }
    tdCreator.innerText = creator;
    tdCreator.title = creator;
    tr.append(tdCreator);

    let tdCateg = document.createElement('td');
    let categ = data.categorieRessource.libelle;
    tdCateg.innerText = categ;
    tdCateg.title = categ;
    tr.append(tdCateg);

    let tdType = document.createElement('td');
    let type = data.typeRessource.libelle;
    tdType.innerText = type;
    tdType.title = type;
    tr.append(tdType);

    let tdCreation = document.createElement('td');
    let createdDate = parseDateToFr(data.createdDate);
    tdCreation.innerText = createdDate;
    tdCreation.title = createdDate;
    tr.append(tdCreation);

    let tdUpdate = document.createElement('td');
    if (data.updatedDate) {
        let updatedDate = parseDateToFr(data.updatedDate);
        tdUpdate.innerText = updatedDate;
        tdUpdate.title = updatedDate;
    }
    tr.append(tdUpdate);

    let tdAction = document.createElement('td');
    let divAction = document.createElement('div');
    divAction.className = "divAction";

    if (!data.deletedDate) {
        let $iEdit = $('<i class="fas fa-edit"></i>');
        $iEdit.click(displayNonFait);
        let $iDel = $('<i class="fas fa-trash"></i>');
        $iDel.click(displayNonFait);
        $(divAction).append($iEdit);
        $(divAction).append($iDel);
    } else {
        divAction.innerText = "Supprimée " + parseDateToFr(data.deletedDate);
    }
    tdAction.append(divAction);

    tr.append(tdAction);

    return tr;
}