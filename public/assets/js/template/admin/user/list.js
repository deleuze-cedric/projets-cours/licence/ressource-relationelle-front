// JS
let table;
let userModel;
let nbItermPerPage = 20;

$(document).ready(function () {
    userModel = new UserModel();
    table = new Table('table-admin-users');

    pagination = new Pagination(nbItermPerPage);
    pagination.addEventListener(Pagination.pageChangeEvent, filterResourcesUsers);

    // Premier filtrage
    filterResourcesUsers();
});

/**
 * Permet de filtrer les ressources
 *
 */
function filterResourcesUsers() {

    // Paramètres envoyés pour la requête api
    let params = {};
    params[Model.LOADER] = "global-loader";

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les catégories de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        pagination.calculNbPage(data);
        table.bindTable(data, createRowUser);
    };
    // Récupération de la liste des catégories ressources
    userModel.callAction(Model.LIST, params);
}

function createRowUser(data) {
    let tr = document.createElement('tr');

    let tdNom = document.createElement('td');
    let nom = data.nom;
    tdNom.innerText = nom;
    tdNom.title = nom;
    tr.append(tdNom);

    let tdPrenom = document.createElement('td');
    let prenom = data.prenom;
    tdPrenom.innerText = prenom;
    tdPrenom.title = prenom;
    tr.append(tdPrenom);

    let tdPseudo = document.createElement('td');
    let pseudo = data.pseudo;
    if (!pseudo) {
        pseudo = "";
    }
    tdPseudo.innerText = pseudo;
    tdPseudo.title = pseudo;
    tr.append(tdPseudo);

    let tdMail = document.createElement('td');
    let mail = data.email;
    tdMail.innerText = mail;
    tdMail.title = mail;
    tr.append(tdMail);

    let tdRole = document.createElement('td');
    let role = UserModel.rolesAvailable[data.roles];
    if(!role){
        role = "";
    }
    tdRole.innerText = role;
    tdRole.title = role;
    tr.append(tdRole);

    let tdCreation = document.createElement('td');
    let createdDate = parseDateToFr(data.createdDate);
    tdCreation.innerText = createdDate;
    tdCreation.title = createdDate;
    tr.append(tdCreation);

    let tdUpdate = document.createElement('td');
    if (data.updatedDate) {
        let updatedDate = parseDateToFr(data.updatedDate);
        tdUpdate.innerText = updatedDate;
        tdUpdate.title = updatedDate;
    }
    tr.append(tdUpdate);

    let tdAction = document.createElement('td');
    let divAction = document.createElement('div');
    divAction.className = "divAction";
    let $iEdit = $('<i class="fas fa-edit"></i>');
    $iEdit.click(displayNonFait);
    let $iDel = $('<i class="fas fa-trash"></i>');
    $iDel.click(displayNonFait);
    $(divAction).append($iEdit);
    $(divAction).append($iDel);
    tdAction.append(divAction);

    tr.append(tdAction);

    return tr;
}