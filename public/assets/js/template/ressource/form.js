let $selectRelationType;
let $formResource;
let $btnCreateResource;
let $resourceTitle;
let $resourceCategory;
let $typeRelationResource;
let $typeRessourceResource;
let $contentResource;
let $urlResource;
let $mediaResource;
let $bodyResource;
let $noBodyResource;
let multipleSelect;
let tinyContent;
let dropzone;

let fieldsTypeResource = {
    "/api/type_ressources/activite-jeu-a-realiser": ["text"],
    "/api/type_ressources/article": ["text", "url"],
    "/api/type_ressources/carte-defi": ["text"],
    "/api/type_ressources/cours-au-format-pdf": ["media", "url"],
    "/api/type_ressources/exercice-atelier": ["text"],
    "/api/type_ressources/fiche-de-lecture": ["text", "url"],
    "/api/type_ressources/jeu-en-ligne": ["url", "media"],
    "/api/type_ressources/video": ["url", "media"],
};

$(document).ready(function () {

    $formResource = $('#RessourceCreation-Form');
    $resourceTitle = $("#resource-title");
    $resourceCategory = $("#resource-category");
    $typeRelationResource = $('#ressource-type-relation');
    $typeRessourceResource = $('#ressource-type');
    $contentResource = $("#input-text");
    $urlResource = $("#input-url");
    $mediaResource = $("#input-media");
    $btnCreateResource = $("#btn-create-resource");
    $bodyResource = $("#bodyCreation");
    $noBodyResource = $("#noTypeChoose");

    // ----------------------- AJAX ------------------------------
    let categorieRessource = new CategorieRessourceModel();
    let typeRelation = new TypeRelation();
    let typeRessource = new TypeRessource();

    // Paramètres envoyés pour la requête api
    let params = {};
    params[Model.LOADER] = "global-loader";

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les catégories de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        Model.parseDataToSelect(Model.getListFromJSONLD(data), "@id", "libelle", "", "", "", false, $resourceCategory);
    };
    // Récupération de la liste des catégories ressources
    categorieRessource.callAction(Model.LIST, params);

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les types de relations
    params[Model.SUCCESS_FUNC] = function (data, status) {
        Model.parseDataToSelect(Model.getListFromJSONLD(data), "@id", "libelle", "", "", "", true, $typeRelationResource);
        // Transformation du select en multiple select
        multipleSelect = new MultipleSelect();
        multipleSelect.id = $typeRelationResource.attr('id');
        multipleSelect.parameter = {'locale': 'fr-FR'}
        multipleSelect.initMultipleSelect();
    };
    // Récupération de la liste des types de relations
    typeRelation.callAction(Model.LIST, params);

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les types de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        Model.parseDataToSelect(Model.getListFromJSONLD(data), "@id", "libelle", "", "", "", false, $typeRessourceResource);
        changeDisplayedFields();
    };
    // Récupération de la liste des types de ressources
    typeRessource.callAction(Model.LIST, params);

    $typeRessourceResource.change(changeDisplayedFields);

    // --------------------- Form -------------------------
    $formResource.submit(createResource);


});

function createResource(e) {

    try {

        // On casse le comportement par défaut
        e.preventDefault();

        $globalLoader.show();

        let ressource = new RessourceModel();

        // ICI récupération des champs
        let title = $resourceTitle.val();
        let category = $resourceCategory.val();
        let typeRelation = $typeRelationResource.val();
        let typeRessources = $typeRessourceResource.val();

        ressource.titre = title;
        ressource.categorieRessource = category;
        ressource.typeRessource = typeRessources;
        ressource.contenu = getContent();

        let params = {};
        // Fonction exécutée au succès de la requête ajax
        params[Model.SUCCESS_FUNC] = function (data, status) {

            let numberOfRq = 0;

            for (let val of typeRelation) {
                let modelPartage = new Partage();
                modelPartage.ciblePartage = val;
                modelPartage.ressourcePartage = data['@id'];

                params[Model.SUCCESS_FUNC] = function () {
                    numberOfRq--;
                    successPartage(numberOfRq, data);
                };

                numberOfRq++;

                modelPartage.callAction(Model.POST, params);

            }

            successPartage(numberOfRq, data);
        };
        // On appel la fonction d'api pour créer la ressource
        ressource.callAction(Model.POST, params);

    } catch (e) {
        let $erreur = $('#' + RequestAjax.prefixError + 'contenu');
        $erreur.html(e);
        showEphemeralMessage($erreur);
        $globalLoader.hide();
    }
}

function successPartage(nbRq, ressource) {

    if (nbRq === 0) {
        window.location.href = "/ressources/modification/" + ressource['slug'];
    }

}

function getContent() {
    let content = {};

    let id = $typeRessourceResource.val();
    // On récupère le type ressource
    let typeRes = fieldsTypeResource[id];

    let notEmpty = false;

    for (let idField of typeRes) {
        let $input = $('#input-' + idField);
        if ($input) {
            let value;
            switch (idField) {
                case 'text':
                    value = tinymce.get($contentResource.attr('id')).getContent();
                    break;
                case 'media':
                    if ($input.get(0) && $input.get(0).files && $input.get(0).files[0]) {
                        let mediaModel = new MediaObject();
                        mediaModel.file = $input.get(0).files[0];
                        mediaModel.postAsync(function (data) {
                            value = apiHTTP + data['contentUrl'];
                        });
                    }
                    break;
                default:
                    value = $input.val();
                    break;
            }

            if(value){
                content[idField] = value;
            }

            notEmpty |= content[idField] && content[idField] !== "";
        }
    }

    if (!notEmpty) {
        throw "Veuillez renseigner au moins un des champs avant de valider le formulaire";
    }

    return content;
}

function changeDisplayedFields() {
    let id = $typeRessourceResource.val();

    // Si une option est sélectionnée dans le select des types de ressources
    if (id && fieldsTypeResource[id]) {
        // On cache tous les champs (text, media, url)
        $bodyResource.find(' > div').hide();
        // On récupère le type ressource
        let typeRes = fieldsTypeResource[id];
        for (let idField of typeRes) {
            let $div = $('#div-' + idField);
            $div.show();
            // Si c'est le input text et que le tinymce n'a pas été initialisé on l'initialise
            if (idField === 'text' && !tinyContent) {
                tinyContent = tinymce.init({
                    selector: '#' + $contentResource.attr('id'),
                    plugins: 'autolink lists media table',
                    language: 'fr_FR',
                    toolbar_persist: true
                });
            } else if (idField === "media") {
                switch (id) {
                    case '/api/type_ressources/cours-au-format-pdf':
                        $mediaResource.attr('accept', '.pdf');
                        break;
                    case '/api/type_ressources/jeu-en-ligne':
                        $mediaResource.attr('accept', '.html');
                        break;
                    case '/api/type_ressources/video':
                        $mediaResource.attr('accept', 'video/*');
                        break;
                }
                // $div.addClass('dropzone');
                // dropzone = $div.dropzone(
                //     {
                //         url: "#",
                //         paramName: "file", // The name that will be used to transfer the file
                //         maxFilesize: 5,
                //         accept: function (file, done) {
                //         }
                //     }
                // );
            }
        }
        $bodyResource.fadeIn();
        $noBodyResource.fadeOut();
    } else {
        $noBodyResource.fadeIn();
        $bodyResource.fadeOut();
    }
}