let $registrationForm;
let $emailRegister;
let $passwordRegister;
let $passwordVerif;
let $firstname;
let $name;
let $pseudo;
let $image;

$(document).ready(function () {

    $registrationForm = $(".registrationForm");
    $emailRegister = $("#input-user-email-register");
    $passwordRegister = $("#input-user-password-register");
    $passwordVerif = $("#input-user-verification-register");
    $firstname = $("#input-user-firstname-register");
    $name = $("#input-user-name-register");
    $pseudo = $("#input-user-pseudo-register");
    $image = $("#change_image");

    // Au submit du form login
    $registrationForm.submit(function (e) {

        // On casse le comportement par défaut
        e.preventDefault();

        // ICI récupération des infos
        let email = $emailRegister.val();
        let nom = $name.val();
        let prenom = $firstname.val();
        let pseudo = $pseudo.val();
        let image = $image.val();
        let mdp = $passwordRegister.val();
        let mdpVerif = $passwordVerif.val();

        if (mdp === mdpVerif) {

            let user = new UserModel();
            user.email = email;
            user.nom = nom;
            user.prenom = prenom;
            user.password = mdp;

            if (pseudo.trim()) {
                user.pseudo = pseudo;
            }

            if (image) {
                user.image = image;
            }

            let params = {};
            // Fonction exécutée au succès de la requête ajax permettant de récupérer les ressources
            params[Model.SUCCESS_FUNC] = successRegistration;
            params[Model.BEFORE_FUNC] = beforeActionModalUser;
            params[Model.ERROR_FUNC] = completeActionModalUser;

            user.callAction(Model.POST, params);

        } else {
            showEphemeralMessage($('#error-password-verif'));
        }

    });

});

function successRegistration(data, success) {

    // ICI récupération email et password
    let password = $passwordRegister.val();
    let email = $emailRegister.val();

    // On connecte l'utilisateur
    user.connect(email, password,
        function () {
            window.location = urlValidAccount;
        },
        completeActionModalUser, null, null, false
    );

}