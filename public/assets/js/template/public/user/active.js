$(document).ready(function (){
    rechargePage();
});

function rechargePage() {
    if (user) {
        user.connection.activationRequired = false;
    }
    setTimeout(function () {
        window.location.href = "/utilisateur/mon-compte";
    }, 2000);

}
