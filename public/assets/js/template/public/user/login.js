let $formLogIn;

$(document).ready(function () {

    $formLogIn = $('.form-login');

    user.connection.addEventListener(Connection.getFailedEvent(), function () {
        showEphemeralMessage($('#erreur-credentials'));
    });

    // Au submit du form login
    $formLogIn.submit(function (e) {

        let $emailLogin = $(this).find('.input-login-email');
        let $passwordLogin = $(this).find('.input-login-password');
        let $rememberMe = $(this).find('.remember-me');

        // On casse le comportement par défaut
        e.preventDefault();

        // ICI récupération email et password
        let password = $passwordLogin.val();
        let email = $emailLogin.val();

        // On connecte l'utilisateur
        user.connect(email, password, null, null, beforeActionModalUser, completeActionModalUser, $rememberMe.val());

    });

});