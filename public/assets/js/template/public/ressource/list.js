// JQuery
let $facetsListResources;
let $blocksFacets;
let $blockFacetsTypeRelation;
let $blockFacetsTypeRessource;
let $blockFacetsCategorieRessource;
let $btnSearchResources;
let $searchUser;
let $searchTitle;
let $sortRes;

// JS
let ressourceModel;
let swiperResource;
let lastIndexPage = -1;
let currentIndexPage = 0;
let prefixIdSwiperPage = "swiper-page-resource-";
let nbItemPerRow = -1;
let nbItemPerColumn = -1;
let $sliderListResources;
let widthSummary = 300;
let heightSummary = 350;
let marginRight = 20;
let marginBottom = 10;
let classSummaryResource = 'summary-resource';
let classSwiperPage = 'swiper-slide';
let filtersChange = false;
let menuBurger;

$(document).ready(function () {

    // Initialisation du model
    ressourceModel = new RessourceModel();
    
    // ----------------------- FACETS ------------------------------

    // Récupération de la div contenant les facets
    $facetsListResources = $('#facets-list-resources');

    menuBurger = new MenuBurger('.menu-burger');
    menuBurger.addEventListener(MenuBurger.getChangeStateEvent(), function (){
        if(menuBurger.isActive()){
            $facetsListResources.addClass('active');
        } else {
            $facetsListResources.removeClass('active');
        }
    });
    
    // Evenement lorsque que la souris est en dehors de la div
    $facetsListResources.on('mouseleave', function () {
        // Si il y a des changementss dans les filtres on refait une recherche
        if (filtersChange) {
            filtersChange = false;
            filterResource(false, null, true);
        }
    });

    // On récupère la div correspondant au slider
    $sliderListResources = $('#slider-list-resources');

    // Au changement de taille de la fenêtre on recalcul le nombre de ressource par page
    $(window).resize(function () {
        calculNbItemPerPage(true);
    });

    // On récupère tous les blocks avec un dropdown
    $blocksFacets = $('.block-facets');

    // Récupération du bouton forçant la recherche
    $btnSearchResources = $('#btn-search-resources');
    // Au click sur le bouton on filtre les résultats
    $btnSearchResources.click(function () {
        filterResource(false, null, true);
    });

    // Au click sur les blocks a facets on les ouvre/ferme
    $blocksFacets.click(changeStateBlockFacet);

    // Permet de changer l'état (ouvert/femé) du dropdown
    function changeStateBlockFacet(e) {
        if (e.target === this || $(e.target).is("h2")) {
            // On récupère la partie contenant les inputs dans ce block
            let $partInput = $(this).find('.part-input-block-facets');
            changeStateDropdown($partInput, $(this));
        }
    }

    // ----------------------- AJAX ------------------------------

    // Initialisation des différents models
    let categorieRessource = new CategorieRessourceModel();
    let typeRelation = new TypeRelation();
    let typeRessource = new TypeRessource();

    $blockFacetsTypeRelation = $('#block-facets-type-relation');
    $blockFacetsTypeRessource = $('#block-facets-type-ressource');
    $blockFacetsCategorieRessource = $('#block-facets-categorie-ressource');
    $searchUser = $('#search-user');
    $searchTitle = $('#search-title');
    $sortRes = $('#sort-res');

    // Démarrage des 3 actions
    let d1 = $.Deferred();
    let d2 = $.Deferred();
    let d3 = $.Deferred();

    // Paramètres envoyés pour la requête api
    let params = {};
    params[Model.LOADER] = "global-loader";

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les catégories de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        let ul = Model.parseDataToCheckbox(Model.getListFromJSONLD(data), "slug", "libelle", "ul-categorie-ressource", "ul-facets", "", "check-categorie-ressource", "", "", true);
        $blockFacetsCategorieRessource.html(ul);
        d1.resolve();
    };
    // Récupération de la liste des catégories ressources
    categorieRessource.callAction(Model.LIST, params);

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les types de relations
    params[Model.SUCCESS_FUNC] = function (data, status) {
        let ul = Model.parseDataToCheckbox(Model.getListFromJSONLD(data), "slug", "libelle", "ul-type-relation", "ul-facets", "", "check-type-relation", "", "", true);
        $blockFacetsTypeRelation.html(ul);
        d2.resolve();
    };
    // Récupération de la liste des types de relations
    typeRelation.callAction(Model.LIST, params);

    // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les types de ressources
    params[Model.SUCCESS_FUNC] = function (data, status) {
        let ul = Model.parseDataToCheckbox(Model.getListFromJSONLD(data), "slug", "libelle", "ul-type-ressource", "ul-facets", "", "check-type-ressource", "", "", true);
        $blockFacetsTypeRessource.html(ul);
        d3.resolve();
    };
    // Récupération de la liste des types de ressources
    typeRessource.callAction(Model.LIST, params);

    // Dès que les 3 actions sont faites on appel la fonction en paramètre
    $.when(d1, d2, d3).done(function () {

        addMouseDownCheckboxSelect();

        // Sur tous les inputs et select, si leur valeur change on passe la variable filtersChange à true
        $facetsListResources.find('input, select').change(function () {
            filtersChange = true;
        });
        $facetsListResources.find('input').keypress(function () {
            filtersChange = true;
        });

    });

    // ----------------------- Ressource ------------------------------

    // Premier filtrage
    filterResource(true, null, false);
});

/**
 * Permet de filtrer les ressources
 *
 * @param init
 * @param functionAfterRequestCalculPage
 * @param forceSearch
 */
function filterResource(init, functionAfterRequestCalculPage, forceSearch) {

    // Si le swiper est renseigné on récupère l'index auquel on se trouve
    if (swiperResource) {
        currentIndexPage = swiperResource.realIndex;
    }

    // Si l'index est différents ou que l'on force la recherche, on exécute la recherche
    if (lastIndexPage !== currentIndexPage || forceSearch) {

        lastIndexPage = currentIndexPage;

        // On cache les pages contenant les ressources
        $('.' + classSwiperPage).fadeOut(300);
        $('#no-result').fadeOut();

        // Si il n'y a pas de fonction exécuté par les calculs (modif css) on met à jourle nombre de résultat possible par page (seulement à l'init)
        if (!functionAfterRequestCalculPage) {
            functionAfterRequestCalculPage = calculNbItemPerPage(false);
        }

        // Calcul du nombre d'item par page (col * row)
        let nbItemPerpage = nbItemPerRow * nbItemPerColumn;

        // Exécution de la fonction de recherche
        ressourceModel.filterResources(
            // Fonction appellée au succès de la requête
            function (data, status) {

                // On initialise le swiper
                if (init) {

                    // Création du Swiper
                    swiperResource = new Swiper('.swiper-container', {
                        // Optional parameters
                        direction: 'horizontal',
                        loop: false,
                        grabCursor: true,

                        // If we need pagination
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                            renderBullet: function (index, className) {
                                return '<span class="' + className + '">' + (index + 1) + '</span>';
                            },
                        },
                        // Navigation arrows
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        },

                        // And if we need scrollbar
                        scrollbar: {
                            el: '.swiper-scrollbar',
                        },
                    });

                    // Lorsque l'on change de page on exécute à nouveau la fonction de filtre
                    swiperResource.on('slideChange', function () {
                        filterResource(false, null, false);
                    });

                }

                // Calcul du nombre de page total nécessaire
                let totalItems = Model.getTotalItemsFromJSONLD(data);
                let totalPage = Math.ceil(totalItems / nbItemPerpage);
                let nbCurrentPage = 0;
                let swiperPages = $('.' + classSwiperPage);

                // On récupère le nombre actuel de page
                if (swiperPages) {
                    nbCurrentPage = swiperPages.length;
                }

                // On fait la diff de page réelles par rapport aux pages voulues
                let delta = totalPage - nbCurrentPage;
                // Delta < 0 = Pages à supprimer
                let deletePage = delta < 0;

                // Si li y a une différence
                if (delta !== 0) {

                    for (let i = 0; i < Math.abs(delta); i++) {
                        if (deletePage) {
                            // Suppression d'une page
                            swiperResource.removeSlide(nbCurrentPage - 1);
                            nbCurrentPage--;
                        } else {
                            // Création d'une page
                            let page = document.createElement('div');
                            page.id = prefixIdSwiperPage + nbCurrentPage;
                            page.className = classSwiperPage;
                            swiperResource.addSlide(nbCurrentPage, page);
                            nbCurrentPage++;
                        }

                    }

                    // Mise à jour de la page actuelle
                    currentIndexPage = swiperResource.realIndex;

                    // Désactivation / Activation de la pagination et de la navigation
                    if (totalPage <= 1) {
                        $('.swiper-wrapper').addClass("disabled");
                        $('.swiper-pagination').addClass("disabled");
                        $('.swiper-button-next').addClass("disabled");
                        $('.swiper-button-prev').addClass("disabled");
                    } else {
                        $('.swiper-wrapper').removeClass("disabled");
                        $('.swiper-pagination').removeClass("disabled");
                        $('.swiper-button-next').removeClass("disabled");
                        $('.swiper-button-prev').removeClass("disabled");
                    }

                }

                // Récupération DOM de la page en cours
                let currentPage = $('#' + prefixIdSwiperPage + currentIndexPage);

                // Vide le contenu de la page
                currentPage.html("");


                // Récupération de la liste de ressource
                data = Model.getListFromJSONLD(data);

                for (let index in data) {
                    // On créé un objet à partir du json et on le transforme en div pour l'ajouter à la page du swiper
                    let ressource = new RessourceModel();
                    ressource.createObjectFromData(data[index]);
                    currentPage.append(ressource.toSummaryDiv());
                }

                // Si la fonction de modifcation de css du calcul est définie on l'exécute
                if (functionAfterRequestCalculPage) {
                    functionAfterRequestCalculPage();
                }

                if (totalItems === 0) {
                    $('#no-result').fadeIn();
                }

                // On fait réapparaitre les pages du swiper
                $('.' + classSwiperPage).fadeIn(300, function (){
                    swiperResource.update();
                });


            },
            // Page à récupérer
            currentIndexPage + 1,
            // Nombre d'item par page
            nbItemPerpage,
            // Récupération des types relations sélectionnées
            getParamUriTypeRelation(),
            // Récupération des types ressource sélectionnées
            getParamUriTypeRessource(),
            // Récupération des catégories ressource sélectionnées
            getParamUriCategorieRessource(),
            // Récupération du filtre par utilisateur
            getParamUriUser(),
            // Récupération du filtre par titre
            getParamUriTitle(),
            // Récupération du choix d'ordonnancement
            getParamUriOrderBy(),
        );
    }
}

/**
 * Récupère les checkbox check pour les relations
 *
 * @returns {string|[]}
 */
function getParamUriTypeRelation() {
    let typeRelationSelected = [];

    // Récup des input
    let $lsTypeRelation = $blockFacetsTypeRelation.find('ul li input');

    // Pour chaque input si il est sélectionné on l'ajoute au tableau
    $lsTypeRelation.each(function () {
        if ($(this).prop('checked')) {
            typeRelationSelected.push(this.value);
        }
    });

    // Si tous inputs sont sélectionnés on retourne rien
    if (typeRelationSelected.length === $lsTypeRelation.length) {
        return "";
    }

    return typeRelationSelected;
}

/**
 * Récupère les checkbox check pour les categories
 *
 * @returns {string|[]}
 */
function getParamUriCategorieRessource() {
    let categoriesRessourcesSelected = [];

    // Récup des input
    let $lsCategRessource = $blockFacetsCategorieRessource.find('ul li input');

    // Pour chaque input si il est sélectionné on l'ajoute au tableau
    $lsCategRessource.each(function () {
        if ($(this).prop('checked')) {
            categoriesRessourcesSelected.push(this.value);
        }
    });

    // Si tous inputs sont sélectionnés on retourne rien
    if (categoriesRessourcesSelected.length === $lsCategRessource.length) {
        return "";
    }


    return categoriesRessourcesSelected;
}

/**
 * Récupère les checkbox check pour les types
 *
 * @returns {string|[]}
 */
function getParamUriTypeRessource() {
    let typeRessourceSelected = [];

    // Récup des input
    let $lsTypeRessource = $blockFacetsTypeRessource.find('ul li input');

    // Pour chaque input si il est sélectionné on l'ajoute au tableau
    $lsTypeRessource.each(function () {
        if ($(this).prop('checked')) {
            typeRessourceSelected.push(this.value);
        }
    });

    // Si tous inputs sont sélectionnés on retourne rien
    if (typeRessourceSelected.length === $lsTypeRessource.length) {
        return "";
    }


    return typeRessourceSelected;
}

/**
 * Récupère la valeur dans le input de choix de l'utilisateur
 *
 * @returns {string|[]}
 */
function getParamUriUser() {
    let val = $searchUser.val();
    if (val && val.trim()) {
        return val;
    }
    return null;
}

/**
 * Récupère la valeur dans le input du titre
 *
 * @returns {string|[]}
 */
function getParamUriTitle() {
    let val = $searchTitle.val();
    if (val && val.trim()) {
        return val;
    }
    return null;
}

/**
 * Récupère la valeur dans le input du titre
 *
 * @returns {string|[]}
 */
function getParamUriOrderBy() {
    let val = $sortRes.val();
    if (val) {
        return val;
    }
    return null;
}

/**
 * Calcul le nombre de ressources par page en fonction de la tailel de l'écran
 * @param resize
 * @returns {functionCalcul}
 */
function calculNbItemPerPage(resize) {

    // Taille et hauteur du swiper
    let widthTotal = $sliderListResources.width();
    let heightTotal = $sliderListResources.height();

    // Padding dans le swiper
    let paddingPage = 100;

    // Nb par ligne et colonne [(taille total - padding du swiper) / (taille + margin entre chaque resssource)]
    let nbWidthSummary = Math.max(Math.trunc((widthTotal - paddingPage) / (widthSummary + marginRight)), 1);
    let nbHeightSummary = Math.max(Math.trunc((heightTotal - paddingPage) / (heightSummary + marginBottom)), 1);

    // Vérifie que le nombre d'item par page a changé
    let nbChange = (nbWidthSummary * nbHeightSummary) !== (nbItemPerColumn * nbItemPerRow);

    // Définition de la distance entre chaque ressource en hauteur et longueur
    // let gapRow = (widthTotal - (nbWidthSummary * widthSummary) - paddingPage) / (nbWidthSummary - 1);
    // let gapCol = (heightTotal - (nbHeightSummary * heightSummary) - paddingPage) / (nbHeightSummary - 1);

    // Définition de la fonction de modification du css retournée
    let functionCalcul = function () {
        changeCSSPage(
            // gapCol, gapRow,
            null, null, null, null);
    };

    // Si le nombre par ligne ou par colonne a changé on met à jour plus d'éléments en css
    if (nbWidthSummary !== nbItemPerRow || nbHeightSummary !== nbItemPerColumn) {

        nbItemPerColumn = nbHeightSummary;
        nbItemPerRow = nbWidthSummary;

        // Définition de la fonction de modification du css retournée
        functionCalcul = function () {
            changeCSSPage(
                // gapCol, gapRow,
                nbWidthSummary, widthSummary, nbHeightSummary, heightSummary);
        };

    }

    if (resize && nbChange) {
        // Si c'est un resize de la page et que le nombre d'éléments a changé on appel la fonction de filtre
        filterResource(false, functionCalcul, true);
    } else if (resize) {
        // Si c'est un resize de la page et que le nombre d'éléments n'a pas changé on met à jour le css
        changeCSSPage(
            // gapCol, gapRow,
            null, null, null, null);
    } else {
        // Sinon on retourne la fonction
        return functionCalcul;
    }

}

/**
 * Fonction changeant le css du swiper
 * @param nbWidthSummary
 * @param widthSummary
 * @param nbHeightSummary
 * @param heightSummary
 */
function changeCSSPage(
    // gapCol, gapRow,
    nbWidthSummary, widthSummary, nbHeightSummary, heightSummary) {
    let $page = $('.' + classSwiperPage);

    // Si renseigné largeur d'une ressource et le nombre de ressource par ligne
    if (nbWidthSummary && widthSummary) {
        $page.css('grid-template-columns', 'repeat(' + nbWidthSummary + ', ' + widthSummary + 'px)');
    }

    // Si renseigné hauteur d'une ressource et le nombre de ressource par colonne
    if (nbHeightSummary && heightSummary) {
        $page.css('grid-template-rows', 'repeat(' + nbHeightSummary + ', ' + heightSummary + 'px)');
    }

    // Si renseigné distance entre chaque ressource
    // if (gapCol && gapRow) {
    //     $page.css('grid-gap', gapCol + 'px ' + gapRow + 'px');
    // }
}