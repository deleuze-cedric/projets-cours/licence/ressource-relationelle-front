let mouseDown = 0;
let $globalLoader;

$(document).ready(function () {

    $globalLoader = $('#global-loader');

    $(document).mouseup(function () {
        --mouseDown;
    });
    $(document).mousedown(function () {
        ++mouseDown;
    });

    addMouseDownCheckboxSelect();

    $('#modal-non-fait .close').click(function (e) {
        $('#modal-non-fait').fadeOut();
    });

    $('.btn-en-dev').click(displayNonFait);

    $('#modal-non-fait').click(function (e) {
        if (this === e.target && e.target.id !== "content-modal-non-fait") {
            $('#modal-non-fait').fadeOut();
        }
    });

    $('.tippy').each(function () {
        let content = $(this).data('tippy');
        let placement = $(this).data('tippy-placement');
        if (!placement) {
            placement = 'right';
        }
        let animations = $(this).data('tippy-animations');
        if (!animations) {
            animations = '';
        }
        let themes = $(this).data('tippy-themes');
        if (!themes) {
            themes = 'light';
        }
        tippy('#' + this.id, {
            content: content,
            placement: placement,
            animation: animations,
            theme: themes,
        });
    })

});

function displayNonFait(e) {
    $('#modal-non-fait').fadeIn();
    e.preventDefault();
    e.stopPropagation();
}

function changeStateDropdown($content, $dropdown) {
    // Si le dropdown est actif est on désactive et vice versa
    if ($dropdown.hasClass('active')) {
        $content.slideUp('easy');
        $dropdown.removeClass('active');
    } else {
        $content.slideDown('easy');
        $dropdown.addClass('active');
    }
}

function addMouseDownCheckboxSelect() {
    let $checkbox = $('.checbkox-hover-select');
    $checkbox.unbind("click", checkboxClickEvent);
    $checkbox.unbind("mouseover", checkboxMouseOverEvent);
    $checkbox.unbind("mousedown", changeStateCheckbox);
    $checkbox.click(checkboxClickEvent);
    $checkbox.mouseover(checkboxMouseOverEvent);
    $checkbox.mousedown(checkboxMouseDownEvent);

    let $label = $checkbox.next('label');
    $label.unbind('onclick', labelCheckboxClick);
    $label.click(labelCheckboxClick);
}

function checkboxMouseDownEvent(e) {
    changeStateCheckbox($(e.target));
}

function checkboxMouseOverEvent(e) {
    if (mouseDown > 0) {
        changeStateCheckbox($(e.target));
    }
}

function checkboxClickEvent(e) {
    e.preventDefault();
}

function changeStateCheckbox($obj) {
    if ($obj.prop('type') === "checkbox") {
        if ($obj.prop('checked')) {
            $obj.prop('checked', false);
        } else {
            $obj.prop('checked', true);
        }
        $obj.trigger('change');
    }
}

function labelCheckboxClick(e) {
    changeStateCheckbox($('#' + $(e.target).prop('for')));
}

function capitalize(string) {
    if (string)
        return string.charAt(0).toUpperCase() + string.slice(1);
    else
        return string;
}

function showEphemeralMessage($element) {
    $element.fadeIn(800, function () {
        setTimeout(function () {
            $element.fadeOut(800);
        }, 3000);
    });
}

function disableInteraction() {
    $('#overlay-disable-interaction').addClass('active');
}

function activeInteraction() {
    $('#overlay-disable-interaction').removeClass('active');
}

function activeBtnLoader($btn = $('.btn-loader')) {
    $btn.addClass('active');
}

function removeBtnLoader($btn = $('.btn-loader')) {
    $btn.removeClass('active');
}

function parseDateToFr(date = null) {
    return "Le " + new Intl.DateTimeFormat('fr', {
        day: "2-digit",
        month: "long",
        year: 'numeric',
        hour: "2-digit",
        minute: "2-digit"
    }).format(new Date(date));
}

function getIndexOfValue(data, value) {
    for (let index in data) {
        if (data[index] === value) {
            return index;
        }
    }
}

if (typeof (String.prototype.trim) === "undefined") {
    String.prototype.trim = function () {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}