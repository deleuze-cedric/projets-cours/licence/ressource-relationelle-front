$(document).ready(function (){

    $('.dropzone').on('dragleave', function(e) {
            $(this).find('input').get(0).files = null;
    });

    $('.dropzone input').change(function (){
        let $parent = $(this).parent('.dropzone');
        $parent.html('');
        if( $(this).get(0).files && $(this).get(0).files[0]){
            $parent.append('<div class="file-dropzone"><i class="far fa-5x fa-file"></i><span>' + $(this).get(0).files[0].name + '</span></div>');
        }
    });

});