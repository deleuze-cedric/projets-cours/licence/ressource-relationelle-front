class EnumHTTPMethods {

    static POST = "POST";
    static GET = "GET";
    static PUT = "PUT";
    static PATCH = "PATCH";
    static DELETE = "DELETE";

}