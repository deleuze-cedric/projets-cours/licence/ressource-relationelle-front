class RequestAjax {

    static debug = true;
    static nbRequestSameTimeForLoader = {};
    static prefixError = "error-";

    url;
    data;
    method = EnumHTTPMethods.GET;
    contentType = EnumContentType.JSON_LD;
    dataType = EnumDataType.JSON_LD;
    successFunction;
    errorFunction;
    beforeFunction;
    completeFunction;
    loaderID;
    needAuthentication;
    async = true;
    processData = true;
    mimeType;

    execute(objectCallFunction = null) {

        if (RequestAjax.debug) {
            console.log(this);
        }

        let token = user.connection.tokenObject.getToken();

        if (this.needAuthentication) {
            if (!token) {
                if (!user.connection.tokenObject.refresh_token && getCookie(userRefreshTokenCookieVar)) {
                    user.connection.tokenObject.refresh_token = getCookie(userRefreshTokenCookieVar);
                }
                if (user.connection.tokenObject.refresh_token) {
                    let request = this;
                    user.connection.tokenObject.refresh(function () {
                        request.doRequest(objectCallFunction, request.loaderID);
                    }, request.loaderID, request.async);
                } else {
                    // TODO mettre une erreur
                }
            } else {
                this.doRequest(objectCallFunction);
            }
        } else {
            this.doRequest(objectCallFunction);
        }

    }

    doRequest(objectCallFunction) {
        let request = this;

        let dataType = request.dataType;

        if (dataType === EnumDataType.JSON_LD) {
            dataType = EnumDataType.TEXT;
        }

        console.log(this.async);

        $.ajax({

            // Adresse de la requête
            url: this.url,

            // Méthode par laquelle est envoyée la requête
            type: this.method,

            // Données envoyées
            data: this.data,

            //
            contentType: this.contentType,

            // Type de données à recevoir
            dataType: dataType,

            async: this.async,

            processData: this.processData,

            mimeType: this.mimeType,

            // Fonction exécutée avant la requête
            beforeSend: function (xhr) {

                if (request.loaderID) {
                    // On initialise le nombre de requête pour ce loader si il n'existe pas dans le tableau
                    if (!RequestAjax.nbRequestSameTimeForLoader[request.loaderID]) {
                        RequestAjax.nbRequestSameTimeForLoader[request.loaderID] = 0;
                    }

                    // On active le loader si c'est la première requête
                    if (RequestAjax.nbRequestSameTimeForLoader[request.loaderID] === 0) {
                        $('#' + request.loaderID).fadeIn();
                    }

                    // Incrémentation du nombre de requête
                    RequestAjax.nbRequestSameTimeForLoader[request.loaderID]++;

                }

                // On met le token pour s'identifier
                if (request.needAuthentication) {
                    let token = user.connection.tokenObject.getToken();
                    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
                }

                // On éxécute la fonction custom
                if (request.beforeFunction) {
                    if (objectCallFunction) {
                        request.beforeFunction.call(objectCallFunction, xhr);
                    } else {
                        request.beforeFunction(xhr);
                    }
                }

            },

            // Fonction exécutée si la requête est un succès (Retour code 200, 201, 202, ...)
            success: function (data, status) {

                if (request.dataType === EnumDataType.JSON_LD) {
                    data = JSON.parse(data);
                }

                if (RequestAjax.debug) {
                    console.log(data);
                }

                // On éxécute la fonction custom
                if (request.successFunction) {
                    if (objectCallFunction) {
                        request.successFunction.call(objectCallFunction, data, status);
                    } else {
                        request.successFunction(data, status);
                    }
                }

            },

            // Fonction exécutée si la requête échoue (Retour code 400, 401, 500, ...)
            error: function (result, status, error) {

                if (RequestAjax.debug) {
                    console.log("Status : " + status);
                    console.log("Error : \n" + error);
                    console.log("Result : ");
                    console.log(result);
                }

                let data = result;

                console.log(data);
                console.log(request.dataType);

                if (request.dataType && request.dataType === EnumDataType.JSON_LD) {
                    data = JSON.parse(result.responseText);
                }

                // Si le token est expiré on le refresh
                if (data && data.code === 401) {

                    if (data.message && data.message === "Expired JWT Token") {
                        // Si le refresh est ok on remplie l'objet token et on reexecute la requête ajax
                        user.connection.tokenObject.refresh(
                            function (data, status) {
                                user.connection.tokenObject.setPropertiesFromData(data);
                                request.execute(objectCallFunction);
                            }
                        );
                    }
                }

                console.log(data);

                if (data['@type'] && data['@type'] === "ConstraintViolationList" && data.violations) {

                    let errors = {};
                    let $errorsDiv = $('.error');
                    $errorsDiv.html('');
                    $errorsDiv.fadeOut();

                    for (let index in data.violations) {
                        let field = data.violations[index];
                        let text = "";
                        if (errors[field]) {
                            text += errors[field] + "<br/>";
                        }
                        text += field.message;
                        errors[field.propertyPath] = text;
                    }

                    console.log(errors);

                    for (let field in errors) {
                        console.log('#' + RequestAjax.prefixError + field);
                        let $field = $('#' + RequestAjax.prefixError + field);
                        $field.html(errors[field]);
                        $field.fadeIn();
                    }

                }

                // On éxécute une fonction custom
                if (request.errorFunction) {
                    if (objectCallFunction) {
                        request.errorFunction.call(objectCallFunction, result, status, error);
                    } else {
                        request.errorFunction(result, status, error);
                    }
                }

            },

            // Fonction exécutée que la requête soit un succès ou un échec
            complete: function (result, status) {

                if (request.loaderID) {

                    // Décrémentation du nombre de requête
                    RequestAjax.nbRequestSameTimeForLoader[request.loaderID]--;

                    // On cache le loader si aucune requête n'est en cours
                    if (RequestAjax.nbRequestSameTimeForLoader[request.loaderID] <= 0) {
                        $("#" + request.loaderID).fadeOut();
                    }

                }

                if (request.completeFunction) {
                    if (objectCallFunction) {
                        request.completeFunction.call(objectCallFunction);
                    } else {
                        request.completeFunction();
                    }
                }

            }

        });
    }

}