class EnumContentType {

    static JSON = "application/json";
    static JSON_LD = "application/ld+json";
    static TEXT = "text/html";
    static MEDIA = "multipart/form-data";

}