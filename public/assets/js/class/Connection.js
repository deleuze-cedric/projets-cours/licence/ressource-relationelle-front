class Connection extends EventTarget {

    email;
    password;
    tokenObject;
    connectSuccessFunction;
    connectFailedFunction;
    connectBeforeFunction;
    connectCompleteFunction;
    loaderID;
    rememberMe;

    constructor() {
        super();
        this.tokenObject = new Token();
        this.successEvent = new CustomEvent(Connection.getSuccessEvent());
        this.errorEvent = new CustomEvent(Connection.getFailedEvent());
        this.addEventListener(Connection.getSuccessEvent(), this.refreshPageAfterLoginSuccess);
    }

    connect() {
        let request = new RequestAjax();
        request.url = apiUrl + "/login_check";
        request.data = JSON.stringify(this);
        request.method = EnumHTTPMethods.POST;
        request.dataType = EnumDataType.JSON;
        request.contentType = EnumContentType.JSON;
        request.successFunction = this.successRequest;
        request.errorFunction = this.errorRequest;
        request.completeFunction = this.connectCompleteFunction;
        request.beforeFunction = this.connectBeforeFunction;
        request.loaderID = this.loaderID;
        request.execute(this);
    }

    successRequest(data, status) {
        this.tokenObject = new Token();
        this.tokenObject.setPropertiesFromData(data);
        if (this.connectSuccessFunction) {
            this.connectSuccessFunction(data, status);
            this.connectSuccessFunction = null;
        }
        this.password = null;

        if(this.rememberMe){
            setCookie(userRefreshTokenCookieVar, data.refresh_token, 60*24*30);
        }

        this.dispatchEvent(this.successEvent);
    }

    errorRequest(result, status, error) {
        if (this.connectFailedFunction) {
            this.connectFailedFunction(result, status, error);
            this.connectFailedFunction = null;
        }
        this.dispatchEvent(this.errorEvent);
    }

    addUserSuccessListener(func) {
        this.removeEventListener(Connection.getSuccessEvent(), this.refreshPageAfterLoginSuccess);
        this.removeEventListener(Connection.getSuccessEvent(), func);
    }

    refreshPageAfterLoginSuccess(e, data, status) {
        let queryString = window.location.search;
        let urlParams = new URLSearchParams(queryString);
        let redirect = urlParams.get('redirect')

        if(redirect){
            document.location.href = redirect;
        } else {
            document.location.reload();
        }
    }

    static getSuccessEvent() {
        return 'connection-success';
    }

    static getFailedEvent() {
        return 'connection-failed';
    }

}