class Token {

    token;
    refresh_token;

    refresh(successFunction = this.successRefresh, loader = null, async = true) {
        let token = this;
        let request = new RequestAjax();
        request.url = apiUrl + "/token/refresh?refresh_token=" + this.refresh_token;
        request.method = EnumHTTPMethods.POST;
        request.dataType = EnumDataType.JSON;
        request.contentType = EnumContentType.TEXT;

        request.async = async;

        request.successFunction = function (data) {
            token.setToken(data.token);
            token.refresh_token = data.refresh_token;
            successFunction(data);
        };
        if (loader) {
            request.loaderID = loader;
        }
        request.execute(this);
    }

    successRefresh(data, status) {
        this.setPropertiesFromData(data);
    }

    setPropertiesFromData(data) {
        this.token = data.token;
        this.refresh_token = data.refresh_token;
        data.user.connection = new Connection();
        data.user.connection.email = user.email;
        data.user.connection.tokenObject = this;
        user.createObjectFromData(data.user);
    }

    getToken() {
        return this.token;
    }

    setToken(token) {
        this.token = token;
    }

}