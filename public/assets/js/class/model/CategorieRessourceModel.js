class CategorieRessourceModel extends Model {

    slug;
    locale;
    libelle;
    createdDate;
    updatedDate;

    constructor() {
        super("categorie_ressources");
    }

    actionAvailableForObject() {
        let obj = {};

        obj[Model.GET] = {};
        obj[Model.GET][Model.AVAILABLE] = true;
        obj[Model.GET][Model.AUTH] = false;

        obj[Model.POST] = {};
        obj[Model.POST][Model.AVAILABLE] = true;
        obj[Model.POST][Model.AUTH] = true;

        obj[Model.PUT] = {};
        obj[Model.PUT][Model.AVAILABLE] = true;
        obj[Model.PUT][Model.AUTH] = true;

        obj[Model.PATCH] = {};
        obj[Model.PATCH][Model.AVAILABLE] = true;
        obj[Model.PATCH][Model.AUTH] = true;

        obj[Model.LIST] = {};
        obj[Model.LIST][Model.AVAILABLE] = true;
        obj[Model.LIST][Model.AUTH] = false;

        return obj;
    }
}
