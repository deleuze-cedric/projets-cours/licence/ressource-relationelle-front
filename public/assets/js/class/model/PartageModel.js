class Partage extends Model {
    partageur;
    ciblePartage;
    ressourcePartage;
    createdDate;
    activatedDate;

    constructor() {
        super("partages");
    }

    actionAvailableForObject() {
        let obj = {};

        obj[Model.POST] = {};
        obj[Model.POST][Model.AVAILABLE] = true;
        obj[Model.POST][Model.AUTH] = true;

        obj[Model.DELETE] = {};
        obj[Model.DELETE][Model.AVAILABLE] = true;
        obj[Model.DELETE][Model.AUTH] = true;

        return obj;
    }
}
