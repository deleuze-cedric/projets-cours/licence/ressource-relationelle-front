class MediaObject extends Model {
    contentUrl;
    file;
    filePath;

    constructor() {
        super("media_objects");
    }

    actionAvailableForObject() {
        let obj = {};

        obj[Model.GET] = {};
        obj[Model.GET][Model.AVAILABLE] = true;
        obj[Model.GET][Model.AUTH] = true;

        obj[Model.POST] = {};
        obj[Model.POST][Model.AVAILABLE] = true;
        obj[Model.POST][Model.AUTH] = true;

        obj[Model.LIST] = {};
        obj[Model.LIST][Model.AVAILABLE] = true;
        obj[Model.LIST][Model.AUTH] = true;

        obj[Model.DELETE] = {};
        obj[Model.DELETE][Model.AVAILABLE] = true;
        obj[Model.DELETE][Model.AUTH] = true;

        return obj;
    }

    postAsync(successFunction = null, errorFunction = null) {

        let form = new FormData();
        form.append("file", this.file);


        let params = {};
        // Fonction exécutée au succès de la requête ajax permettant de créeer la selection pour les catégories de ressources
        params[Model.NO_ASYNC] = true;u
        params[Model.CONTENT_TYPE] = false;
        params[Model.MIME_TYPE] = EnumContentType.MEDIA;
        params[Model.DATA] = form;
        params[Model.NO_PROCESS_DATA] = true;
        params[Model.SUCCESS_FUNC] = successFunction;
        params[Model.ERROR_FUNC] = errorFunction;
        this.callAction(Model.POST, params);

    }
}