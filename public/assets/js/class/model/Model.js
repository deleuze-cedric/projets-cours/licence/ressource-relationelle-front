class Model extends EventTarget {

    // Liste des actions possibles de faire sur un objet
    static GET = "get";
    static LIST = "getList";
    static POST = "create";
    static PUT = "updateEntire";
    static PATCH = "updatePartial";

    // Libellé des différents pouvant être envoyés dans la variable param pour les requêtes
    static AUTH = "needAuthentication";
    static AVAILABLE = "available";
    static SUCCESS_FUNC = "functionSuccess";
    static ERROR_FUNC = "functionError";
    static BEFORE_FUNC = "functionBefore";
    static COMPLETE_FUNC = "functionComplete";
    static LOADER = "loader";
    static URL_PARAMTERS = "getParameters";
    static NO_ASYNC = "noAsync";
    static CONTENT_TYPE = "content-type";
    static DATA = "data";
    static NO_PROCESS_DATA = "process-data";
    static MIME_TYPE = "mime-type";

    // Url pour accéder aux actions d'une class (Exemple : /api/users, urlInApi vaudra users)
    urlInApi

    /**
     * @param urlInApi
     */
    constructor(urlInApi) {
        super();
        // apiUrl vaut nom_de_domaine/api
        // Exemple https://api_res.local.fr + / + users
        this.urlInApi = apiUrl + "/" + urlInApi;
        // Différentes variables dispo dans les réponses de la BDD
        this['@id'] = null;
        this['@type'] = null;
        this['@context'] = null;
    }

    /**
     * Permet d'exécuter une fonction avec certains paramètres (la fonction doit être active et disponible => voir actionAvailableForObject())
     *
     * @param actionCall (Voir la liste des actions disponibles dans les variables static de la class Model)
     * @param params
     */
    callAction(actionCall, params = {}) {
        // On vérifie si l'action est bien valide pour la class
        if (this.isActionParam(actionCall, Model.AVAILABLE)) {
            // On iniliase les paramètres si ce n'est pas fait
            if (!params) {
                params = {};
            }
            if (!params[Model.AUTH]) {
                // On rajoute le champ auth si une authentification est nécessaire pour la requete vers l'api
                params[Model.AUTH] = this.isActionParam(actionCall, Model.AUTH);
            }
            // On appel la fonction concernée
            return this[actionCall](params);
        } else {
            console.log("Action " + actionCall + " non disponible");
        }
    }

    /**
     * Permet de définir quels actions sont disponible pour la class ou pas (Voir ce qui est dans l'API)
     * Retourne un objet qui pour chaque actions (voir action models) définit si disponible ou pas et si requiert la connexion (Voir UserModel)
     */
    actionAvailableForObject() {
        return {};
    }

    isActionParam(action, param) {
        let actions = this.actionAvailableForObject();
        return actions && actions[action] && actions[action][param];
    }

    get(params) {
        if (this['@id']) {
            let request = this.prepareRequest(
                this.urlInApi + "/" + this['@id'],
                EnumHTTPMethods.GET,
                params
            );
            return request.execute();
        } else {
            return false;
        }
    }

    getList(params) {
        let url = this.urlInApi;

        if (params[Model.URL_PARAMTERS]) {
            let urlParameters = "";
            for (let nameParameter in params[Model.URL_PARAMTERS]) {
                let value = params[Model.URL_PARAMTERS][nameParameter];

                if (Array.isArray(value)) {
                    for (let val of value) {
                        if (urlParameters) {
                            urlParameters += "&";
                        }
                        urlParameters += nameParameter + "=" + val;
                    }
                } else {
                    if (urlParameters) {
                        urlParameters += "&";
                    }
                    urlParameters += nameParameter + "=" + value;
                }

            }
            if (urlParameters) {
                url += "?" + urlParameters;
            }
        }


        let request = this.prepareRequest(
            encodeURI(url),
            EnumHTTPMethods.GET,
            params
        );

        // On ajoute le num de la page si il est renseigné
        if (typeof params.page !== "undefined" && params.page) {
            request.url += "?page=" + params.page;
        }

        return request.execute();
    }

    create(params) {
        if (this['@id']) {
            this.updateEntire();
        } else {
            let request = this.prepareRequest(
                this.urlInApi,
                EnumHTTPMethods.POST,
                params
            );
            if(params[Model.DATA]){
                request.data = params[Model.DATA];
            } else {
                request.data = JSON.stringify(this);
            }
            return request.execute();
        }
        return false;
    }

    updateEntire(params) {
        if (!this['@id']) {
            this.create();
        } else {
            let request = this.prepareRequest(
                this.urlInApi + '/' + this['@id'],
                EnumHTTPMethods.PUT,
                params
            );
            request.data = JSON.stringify(this);
            return request.execute();
        }
        return false;
    }

    updatePartial(params) {
        if (this.id) {
            let request = this.prepareRequest(
                this.urlInApi + '/' + this['@id'],
                EnumHTTPMethods.PATCH,
                params
            );
            request.data = JSON.stringify(params.object);
            return request.execute();
        }
        return false;
    }

    delete(params) {
        if (this['@id']) {
            let request = this.prepareRequest(
                this.urlInApi + '/' + this['@id'],
                EnumHTTPMethods.DELETE,
                params
            );
            return request.execute();
        }
        return false;
    }

    prepareRequest(url, method, params) {
        let request = new RequestAjax();
        request.url = url;
        request.method = method;
        request.contentType = EnumContentType.JSON_LD;
        if (params[Model.NO_ASYNC]) {
            request.async = false;
        }
        if (params[Model.NO_PROCESS_DATA]) {
            request.processData = false;
        }
        if (params[Model.AUTH]) {
            request.needAuthentication = true;
        }
        if (params[Model.CONTENT_TYPE] || params[Model.CONTENT_TYPE] === false) {
            console.log('test');
            request.contentType = params[Model.CONTENT_TYPE];
        }
        if (params[Model.BEFORE_FUNC]) {
            request.beforeFunction = params[Model.BEFORE_FUNC];
        }
        if (params[Model.COMPLETE_FUNC]) {
            request.completeFunction = params[Model.COMPLETE_FUNC];
        }
        if (params[Model.ERROR_FUNC]) {
            request.errorFunction = params[Model.ERROR_FUNC];
        }
        if (params[Model.SUCCESS_FUNC]) {
            request.successFunction = params[Model.SUCCESS_FUNC];
        }
        if (params[Model.LOADER]) {
            request.loaderID = params[Model.LOADER];
        }
        if (params[Model.MIME_TYPE]) {
            request.mimeType = params[Model.MIME_TYPE];
        }
        return request;
    }

    createObjectFromData(data) {
        for (let index in data) {
            this[index] = data[index];
        }
    }

    getUrlInApi() {
        return this.urlInApi;
    }

    static parseDataToSelect(data, idField, textField, idSelect, classesSelect, classesOption, multiselect, select = null) {

        if (!select) {
            select = document.createElement('select');
            select.id = idSelect;
            select.className = classesSelect;
        }

        select = $(select);

        if (multiselect) {
            select.attr('multiple', "multiple");
        }

        for (let index in data) {
            let object = data[index];
            let option = document.createElement('option');
            option.value = object[idField];
            option.innerText = object[textField];
            option.className = classesOption;
            select.append(option);
        }

        return select;
    }

    static parseDataToCheckbox(data, idField, textField, idUl, classesUl, classesLi, nameChecbkox, classesCheckbox, classesLabel, checked = false) {
        let ul = document.createElement('ul');
        ul.id = idUl;
        ul.className = classesUl;

        for (let index in data) {
            let object = data[index];
            let id = nameChecbkox + "_" + index;

            let checkbox = document.createElement('input');
            checkbox.type = "checkbox";
            checkbox.className = "checbkox-hover-select " + classesCheckbox;
            checkbox.name = nameChecbkox;
            checkbox.value = object[idField];
            checkbox.id = id;
            $(checkbox).attr('checked', checked);

            let label = document.createElement('label');
            $(label).prop('for', id);
            label.innerText = object[textField];
            label.title = object[textField];
            label.className = classesLabel + " " + object[idField];

            let li = document.createElement('li');
            li.className = classesLi;
            li.append(checkbox);
            li.append(label);
            ul.append(li);
        }

        return ul;
    }

    static getListFromJSONLD(data) {
        return data['hydra:member'];
    }

    static getTotalItemsFromJSONLD(data) {
        return data['hydra:totalItems'];
    }

}
