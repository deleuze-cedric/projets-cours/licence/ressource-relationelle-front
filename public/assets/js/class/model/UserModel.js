class UserModel extends Model {

    uuid;
    email;
    roles = [];
    nom;
    prenom;
    pseudo;
    image;
    createdDate;
    updatedDate;
    deletedDate;
    resetDate;
    activatedDate;
    connection;
    activationRequired;

    static rolesAvailable = {
        ROLE_USER: "Compte non activé",
        ROLE_CITOYEN: "Citoyen",
        ROLE_MODO: "Modérateur",
        ROLE_ADMIN: "Administrateur",
        ROLE_SUPER_ADMIN: "Super Administrateur"
    };

    actionAvailableForObject() {
        let obj = {};

        obj[Model.GET] = {};
        obj[Model.GET][Model.AVAILABLE] = true;
        obj[Model.GET][Model.AUTH] = true;

        obj[Model.POST] = {};
        obj[Model.POST][Model.AVAILABLE] = true;
        obj[Model.POST][Model.AUTH] = false;

        obj[Model.PUT] = {};
        obj[Model.PUT][Model.AVAILABLE] = true;
        obj[Model.PUT][Model.AUTH] = true;

        obj[Model.PATCH] = {};
        obj[Model.PATCH][Model.AVAILABLE] = true;
        obj[Model.PATCH][Model.AUTH] = true;

        obj[Model.LIST] = {};
        obj[Model.LIST][Model.AVAILABLE] = true;
        obj[Model.LIST][Model.AUTH] = true;

        obj[Model.DELETE] = {};
        obj[Model.DELETE][Model.AVAILABLE] = true;
        obj[Model.DELETE][Model.AUTH] = true;

        return obj;
    }

    constructor() {
        super("users");
        this.connection = new Connection();
    }

    createObjectFromData(data) {

        let email = "";
        let token = "";
        let refreshToken = "";

        if (data.connection) {
            email = data.connection.email;
            if (data.connection.tokenObject) {
                token = data.connection.tokenObject.token;
                if (!token) {
                    token = "";
                }
                refreshToken = data.connection.tokenObject.refresh_token;
            }
        }

        super.createObjectFromData(data);

        this.connection = new Connection();
        this.connection.email = email;
        this.connection.tokenObject.setToken(token);
        this.connection.tokenObject.refresh_token = refreshToken;

        this.increaseCookieTime();

        if (this.activationRequired) {
            if (urlValidAccount !== window.location.href && !window.location.href.includes(urlActiveAccount))
                window.location.href = urlValidAccount;
        }
    }

    sendMailActiveAccount() {
        let request = new RequestAjax();
        request.url = this.getUrlInApi() + "/" + this.uuid + "/envoie_mail_active_compte";
        request.method = EnumHTTPMethods.GET;
        request.dataType = EnumDataType.JSON;
        request.contentType = EnumContentType.JSON_LD;
        request.execute(this);
    }

    increaseCookieTime() {
        let token = this.connection.tokenObject.token;
        this.connection.tokenObject.token = "";
        setCookie(userCookieVar, JSON.stringify(this), 15);
        this.connection.tokenObject.token = token;
    }

    registration() {
        super.create();
    }

    // Connexion de l'utilisateur
    connect(email, password, successLogin = null, failedLogin = null, beforeFunction = null, completeFunction = null, loader = null, rememberMe = false) {
        this.connection.email = email;
        this.connection.password = password;
        this.connection.connectSuccessFunction = successLogin;
        this.connection.connectFailedFunction = failedLogin;
        this.connection.connectBeforeFunction = beforeFunction;
        this.connection.connectCompleteFunction = completeFunction;
        this.connection.loaderID = loader;
        this.connection.rememberMe = rememberMe;
        this.connection.connect();
    }

    disconnect() {
        setCookie(userCookieVar, "", 0);
        document.location.reload();
    }
}