class CommentaireModel extends Model {
    contenu;
    ecritPour;
    ecritPar;
    commentaireParent;
    lsCommentairesEnfants;
    createdDate;
    updatedDate;
    activatedDate;
    deletedDate;

    constructor() {
        super("commentaires");
    }
}
