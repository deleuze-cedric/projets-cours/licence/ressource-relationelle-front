class Discussion extends Model {
    slug;
    titre;
    createur;
    participants;
    ressourceSupport;
    createdDate;
    updatedDate;
    deletedDate;

    constructor() {
        super("discussions");
    }
}
