class Pagination extends EventTarget {

    static pageChangeEvent = 'changePage';
    lastPage = -1;
    currentPage = 0;
    nbPageTotal = 0;
    nbItemPerPage;
    dataPagination = 'numPage';
    classPagination = "pagination-number";

    constructor(nbItemPerpage = 10) {
        super();
        this.pageChange = new CustomEvent(Pagination.pageChangeEvent, {
            currentPage: this.currentPage,
            lastPage: this.lastPage
        });
        this.nbItemPerPage = nbItemPerpage;

        let pagination = this;
        $(".first").click(function () {
            pagination.changePage(0);
        });
        $(".previous").click(function () {
            pagination.previousPage();
        });
        $(".next").click(function () {
            pagination.nextPage();
        });
        $(".last").click(function () {
            pagination.changePage(pagination.nbPageTotal - 1);
        });
    }

    calculNbPage(data) {
        let totalItems = Model.getTotalItemsFromJSONLD(data);
        let totalPage = Math.ceil(totalItems / this.nbItemPerPage);

        // On fait la diff de page réelles par rapport aux pages voulues
        let delta = totalPage - this.nbPageTotal;
        // Delta < 0 = Pages à supprimer
        let deletePage = delta < 0;

        // Si li y a une différence
        if (delta !== 0) {

            for (let i = 0; i < Math.abs(delta); i++) {
                if (deletePage) {
                    this.nbPageTotal--;
                    let idPagination = "page-" + this.nbPageTotal;
                    // Suppression d'une page
                    $('#' + idPagination).remove();
                } else {
                    let idPagination = "page-" + this.nbPageTotal;
                    // Création d'une page
                    let page = document.createElement('div');
                    page.className = this.classPagination + " " + idPagination;
                    page.innerText = (this.nbPageTotal + 1).toString();
                    $(page).data(this.dataPagination, this.nbPageTotal);

                    let pagination = this;
                    $(page).click(function () {
                        pagination.changePage($(this).data(pagination.dataPagination));
                    });

                    $('.lsPage').append(page);
                    this.nbPageTotal++;
                }
            }
        }

        this.changeActivePage();
    }

    changeActivePage() {
        $('.' + this.classPagination).removeClass('active');
        $('.page-' + this.currentPage).addClass('active');
    }

    nextPage() {
        this.changePage(this.currentPage + 1);
    }

    previousPage() {
        this.changePage(this.currentPage - 1);
    }

    changePage(numPage) {
        if (numPage < 0) {
            numPage = 0;
        } else if (numPage >= this.nbPageTotal) {
            numPage = this.nbPageTotal - 1;
        }

        if (numPage !== this.currentPage) {

            this.lastPage = this.currentPage;
            this.currentPage = numPage;

            this.changeActivePage();

            this.dispatchEvent(this.pageChange);

        }
    }
}